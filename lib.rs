#![no_std]

#[no_mangle]
pub static mut musl_alloc_errno: usize = 0;

extern "C" {
    pub fn free(ptr: *mut u8);
    pub fn malloc(size: usize) -> *mut u8;
    pub fn calloc(nmemb: usize, size: usize) -> *mut u8;
    pub fn realloc(ptr: *mut u8, size: usize) -> *mut u8;
    pub fn memalign(align: usize, len: usize) -> *mut u8;
    pub fn malloc_usable_size(ptr: *mut u8) -> usize;
}
