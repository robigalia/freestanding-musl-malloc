#ifndef SHIM_H
#define SHIM_H

#define weak_alias(old, new) \
    extern __typeof(old) new __attribute__((weak, alias(#old)))

#define _Addr int
#define _Reg int
#define _Int64 long long

#define SIZE_MAX (-1)

typedef unsigned _Addr size_t;
typedef unsigned _Addr uintptr_t;
typedef _Addr ptrdiff_t;
typedef _Addr ssize_t;
typedef _Addr intptr_t;
typedef _Addr regoff_t;
typedef _Reg register_t;
typedef signed char     int8_t;
typedef short           int16_t;
typedef int             int32_t;
typedef _Int64          int64_t;
typedef _Int64          intmax_t;
typedef unsigned char   uint8_t;
typedef unsigned short  uint16_t;
typedef unsigned int    uint32_t;
typedef unsigned _Int64 uint64_t;
typedef unsigned _Int64 u_int64_t;
typedef unsigned _Int64 uintmax_t;


typedef int8_t int_fast8_t;
typedef int64_t int_fast64_t;

typedef int8_t  int_least8_t;
typedef int16_t int_least16_t;
typedef int32_t int_least32_t;
typedef int64_t int_least64_t;

typedef uint8_t uint_fast8_t;
typedef uint64_t uint_fast64_t;

typedef uint8_t  uint_least8_t;
typedef uint16_t uint_least16_t;
typedef uint32_t uint_least32_t;
typedef uint64_t uint_least64_t;

#define INT8_MIN   (-1-0x7f)
#define INT16_MIN  (-1-0x7fff)
#define INT32_MIN  (-1-0x7fffffff)
#define INT64_MIN  (-1-0x7fffffffffffffff)

#define INT8_MAX   (0x7f)
#define INT16_MAX  (0x7fff)
#define INT32_MAX  (0x7fffffff)
#define INT64_MAX  (0x7fffffffffffffff)

#define UINT8_MAX  (0xff)
#define UINT16_MAX (0xffff)
#define UINT32_MAX (0xffffffffu)
#define UINT64_MAX (0xffffffffffffffffu)

#define INT_FAST8_MIN   INT8_MIN
#define INT_FAST64_MIN  INT64_MIN

#define INT_LEAST8_MIN   INT8_MIN
#define INT_LEAST16_MIN  INT16_MIN
#define INT_LEAST32_MIN  INT32_MIN
#define INT_LEAST64_MIN  INT64_MIN

#define INT_FAST8_MAX   INT8_MAX
#define INT_FAST64_MAX  INT64_MAX

#define INT_LEAST8_MAX   INT8_MAX
#define INT_LEAST16_MAX  INT16_MAX
#define INT_LEAST32_MAX  INT32_MAX
#define INT_LEAST64_MAX  INT64_MAX

#define UINT_FAST8_MAX  UINT8_MAX
#define UINT_FAST64_MAX UINT64_MAX

#define UINT_LEAST8_MAX  UINT8_MAX
#define UINT_LEAST16_MAX UINT16_MAX
#define UINT_LEAST32_MAX UINT32_MAX
#define UINT_LEAST64_MAX UINT64_MAX

#define INTMAX_MIN  INT64_MIN
#define INTMAX_MAX  INT64_MAX
#define UINTMAX_MAX UINT64_MAX

#define WINT_MIN 0U
#define WINT_MAX UINT32_MAX

#if L'\0'-1 > 0
#define WCHAR_MAX (0xffffffffu+L'\0')
#define WCHAR_MIN (0+L'\0')
#else
#define WCHAR_MAX (0x7fffffff+L'\0')
#define WCHAR_MIN (-1-0x7fffffff+L'\0')
#endif

#define SIG_ATOMIC_MIN  INT32_MIN
#define SIG_ATOMIC_MAX  INT32_MAX

#define INT8_C(c)  c
#define INT16_C(c) c
#define INT32_C(c) c

#define UINT8_C(c)  c
#define UINT16_C(c) c
#define UINT32_C(c) c ## U

#if UINTPTR_MAX == UINT64_MAX
#define INT64_C(c) c ## L
#define UINT64_C(c) c ## UL
#define INTMAX_C(c)  c ## L
#define UINTMAX_C(c) c ## UL
#else
#define INT64_C(c) c ## LL
#define UINT64_C(c) c ## ULL
#define INTMAX_C(c)  c ## LL
#define UINTMAX_C(c) c ## ULL
#endif

#define errno musl_alloc_errno

extern size_t errno;

#define NULL ((void*)0)

#define EINVAL -1
#define ENOMEM -2

typedef ssize_t off_t;

void *malloc(size_t size);
void free(void *ptr);
void *calloc(size_t nmemb, size_t size);
void *realloc(void *ptr, size_t size);
void *mmap(void *, size_t, int, int, int, off_t);
int munmap(void *, size_t);
void *mremap(void *, size_t, size_t, int, ...);
int madvise(void *, size_t, int);


#define PAGE_SIZE 4096

#define PROT_NONE 0x0
#define PROT_READ 0x1
#define PROT_WRITE 0x2
#define PROT_EXEC 0x4

#define MAP_PRIVATE 0x1
#define MAP_ANONYMOUS 0x2
#define MAP_SHARED 0x4
#define MAP_FAILED NULL

#define MREMAP_MAYMOVE 0x1

#define MADV_DONTNEED 0x1

#define PTRDIFF_MAX

#ifdef __x86_64__
#include "x86_64/atomic_arch.h"
#endif

#ifdef __arm__
#include "arm/atomic_arch.h"
#endif

#ifdef __i386__
#include "i386/atomic_arch.h"
#endif

#ifdef a_ll

#ifndef a_pre_llsc
#define a_pre_llsc()
#endif

#ifndef a_post_llsc
#define a_post_llsc()
#endif

#ifndef a_cas
#define a_cas a_cas
static inline int a_cas(volatile int *p, int t, int s)
{
    int old;
    a_pre_llsc();
    do old = a_ll(p);
    while (old==t && !a_sc(p, s));
    a_post_llsc();
    return old;
}
#endif

#ifndef a_swap
#define a_swap a_swap
static inline int a_swap(volatile int *p, int v)
{
    int old;
    a_pre_llsc();
    do old = a_ll(p);
    while (!a_sc(p, v));
    a_post_llsc();
    return old;
}
#endif

#ifndef a_fetch_add
#define a_fetch_add a_fetch_add
static inline int a_fetch_add(volatile int *p, int v)
{
    int old;
    a_pre_llsc();
    do old = a_ll(p);
    while (!a_sc(p, (unsigned)old + v));
    a_post_llsc();
    return old;
}
#endif

#ifndef a_fetch_and
#define a_fetch_and a_fetch_and
static inline int a_fetch_and(volatile int *p, int v)
{
    int old;
    a_pre_llsc();
    do old = a_ll(p);
    while (!a_sc(p, old & v));
    a_post_llsc();
    return old;
}
#endif

#ifndef a_fetch_or
#define a_fetch_or a_fetch_or
static inline int a_fetch_or(volatile int *p, int v)
{
    int old;
    a_pre_llsc();
    do old = a_ll(p);
    while (!a_sc(p, old | v));
    a_post_llsc();
    return old;
}
#endif

#endif

#ifdef a_ll_p

#ifndef a_cas_p
#define a_cas_p a_cas_p
static inline void *a_cas_p(volatile void *p, void *t, void *s)
{
    void *old;
    a_pre_llsc();
    do old = a_ll_p(p);
    while (old==t && !a_sc_p(p, s));
    a_post_llsc();
    return old;
}
#endif

#endif

#ifndef a_cas
#error missing definition of a_cas
#endif

#ifndef a_swap
#define a_swap a_swap
static inline int a_swap(volatile int *p, int v)
{
    int old;
    do old = *p;
    while (a_cas(p, old, v) != old);
    return old;
}
#endif

#ifndef a_fetch_add
#define a_fetch_add a_fetch_add
static inline int a_fetch_add(volatile int *p, int v)
{
    int old;
    do old = *p;
    while (a_cas(p, old, (unsigned)old+v) != old);
    return old;
}
#endif

#ifndef a_fetch_and
#define a_fetch_and a_fetch_and
static inline int a_fetch_and(volatile int *p, int v)
{
    int old;
    do old = *p;
    while (a_cas(p, old, old&v) != old);
    return old;
}
#endif
#ifndef a_fetch_or
#define a_fetch_or a_fetch_or
static inline int a_fetch_or(volatile int *p, int v)
{
    int old;
    do old = *p;
    while (a_cas(p, old, old|v) != old);
    return old;
}
#endif

#ifndef a_and
#define a_and a_and
static inline void a_and(volatile int *p, int v)
{
    a_fetch_and(p, v);
}
#endif

#ifndef a_or
#define a_or a_or
static inline void a_or(volatile int *p, int v)
{
    a_fetch_or(p, v);
}
#endif

#ifndef a_inc
#define a_inc a_inc
static inline void a_inc(volatile int *p)
{
    a_fetch_add(p, 1);
}
#endif

#ifndef a_dec
#define a_dec a_dec
static inline void a_dec(volatile int *p)
{
    a_fetch_add(p, -1);
}
#endif

#ifndef a_store
#define a_store a_store
static inline void a_store(volatile int *p, int v)
{
#ifdef a_barrier
    a_barrier();
    *p = v;
    a_barrier();
#else
    a_swap(p, v);
#endif
}
#endif

#ifndef a_barrier
#define a_barrier a_barrier
static void a_barrier()
{
    volatile int tmp = 0;
    a_cas(&tmp, 0, 0);
}
#endif

#ifndef a_spin
#define a_spin a_barrier
#endif

#ifndef a_and_64
#define a_and_64 a_and_64
static inline void a_and_64(volatile uint64_t *p, uint64_t v)
{
    union { uint64_t v; uint32_t r[2]; } u = { v };
    if (u.r[0]+1) a_and((int *)p, u.r[0]);
    if (u.r[1]+1) a_and((int *)p+1, u.r[1]);
}
#endif

#ifndef a_or_64
#define a_or_64 a_or_64
static inline void a_or_64(volatile uint64_t *p, uint64_t v)
{
    union { uint64_t v; uint32_t r[2]; } u = { v };
    if (u.r[0]) a_or((int *)p, u.r[0]);
    if (u.r[1]) a_or((int *)p+1, u.r[1]);
}
#endif

#ifndef a_cas_p
typedef char a_cas_p_undefined_but_pointer_not_32bit[-sizeof(char) == 0xffffffff ? 1 : -1];
#define a_cas_p a_cas_p
static inline void *a_cas_p(volatile void *p, void *t, void *s)
{
    return (void *)a_cas((volatile int *)p, (int)t, (int)s);
}
#endif

#ifndef a_or_l
#define a_or_l a_or_l
static inline void a_or_l(volatile void *p, long v)
{
    if (sizeof(long) == sizeof(int)) a_or(p, v);
    else a_or_64(p, v);
}
#endif

#ifndef a_crash
#define a_crash a_crash
static inline void a_crash()
{
    *(volatile char *)0=0;
}
#endif

#ifndef a_ctz_64
#define a_ctz_64 a_ctz_64
static inline int a_ctz_64(uint64_t x)
{
    static const char debruijn64[64] = {
        0, 1, 2, 53, 3, 7, 54, 27, 4, 38, 41, 8, 34, 55, 48, 28,
        62, 5, 39, 46, 44, 42, 22, 9, 24, 35, 59, 56, 49, 18, 29, 11,
        63, 52, 6, 26, 37, 40, 33, 47, 61, 45, 43, 21, 23, 58, 17, 10,
        51, 25, 36, 32, 60, 20, 57, 16, 50, 31, 19, 15, 30, 14, 13, 12
    };
    static const char debruijn32[32] = {
        0, 1, 23, 2, 29, 24, 19, 3, 30, 27, 25, 11, 20, 8, 4, 13,
        31, 22, 28, 18, 26, 10, 7, 12, 21, 17, 9, 6, 16, 5, 15, 14
    };
    if (sizeof(long) < 8) {
        uint32_t y = x;
        if (!y) {
            y = x>>32;
            return 32 + debruijn32[(y&-y)*0x076be629 >> 27];
        }
        return debruijn32[(y&-y)*0x076be629 >> 27];
    }
    return debruijn64[(x&-x)*0x022fdd63cc95386dull >> 58];
}
#endif

#ifndef a_ctz_l
#define a_ctz_l a_ctz_l
static inline int a_ctz_l(unsigned long x)
{
    static const char debruijn32[32] = {
        0, 1, 23, 2, 29, 24, 19, 3, 30, 27, 25, 11, 20, 8, 4, 13,
        31, 22, 28, 18, 26, 10, 7, 12, 21, 17, 9, 6, 16, 5, 15, 14
    };
    if (sizeof(long) == 8) return a_ctz_64(x);
    return debruijn32[(x&-x)*0x076be629 >> 27];
}
#endif

extern void *memcpy(void *dest, const void *src, size_t n);

#endif /* SHIM_H */
