A malloc implementation pulled out of musl-libc and made freestanding.
Importantly, all notion of thread safety was removed - this crate can only be
used in single-threaded programs!

Depends on the symbols `memcpy`, `mmap`, `mremap`, `munmap`, and `madvise` being
defined. Defines own errno, named `musl_alloc_errno`.
